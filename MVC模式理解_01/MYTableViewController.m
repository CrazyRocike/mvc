//
//  TableViewController.m
//  MVC模式理解_01
//
//  Created by SuAn on 15/8/4.
//  Copyright (c) 2015年 SuAn. All rights reserved.
//

#import "MYTableViewController.h"
#import "GoodsData.h"
#import "Goods.h"
/*
 kvo监听的是对象指针的变动，NSString、int、float等对象的变动(abc = @"123"、abc = 12、abc = 12.2)皆为指针的变动，所以通过此方式来捕捉array的变化是不可行的
 不能这样 [_model.modelArray addObject]方法，
 需要这样调用   [[_model mutableArrayValueForKey:@"modelArray"] addObject:str];原因稍后说明。
 
 */

@interface MYTableViewController ()

@end

@implementation MYTableViewController{

    GoodsData *goodData;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    
    goodData = [[GoodsData alloc] init];
    [goodData addObserver:self forKeyPath:@"goodsData" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:nil];
    
    NSMutableArray *goods = [NSMutableArray array];
    for (NSString *name in @[@"你好",@"厉害",@"女神"]) {
        Goods *good = [[Goods alloc] init];
        good.name = name;
        [goods addObject:good];
    }
    goodData.goodsData = goods;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    // Return the number of rows in the section.
    return goodData.goodsData.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    Goods *goods = goodData.goodsData[indexPath.row];
    cell.textLabel.text = goods.name;
    
    return cell;
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{

    NSLog(@"指针-01 %p",goodData.goodsData);
    [self.tableView reloadData];

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row <= goodData.goodsData.count - 1) {
        [[goodData mutableArrayValueForKey:@"goodsData"] removeObjectAtIndex:indexPath.row];
        NSLog(@"指针-02 %p",goodData.goodsData);
        NSLog(@"数据%@",goodData.goodsData);
    }
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
