//
//  AppDelegate.h
//  MVC模式理解_01
//
//  Created by SuAn on 15/8/4.
//  Copyright (c) 2015年 SuAn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

