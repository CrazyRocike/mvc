//
//  GoodsData.h
//  MVC模式理解_01
//
//  Created by SuAn on 15/8/4.
//  Copyright (c) 2015年 SuAn. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GoodsData : NSObject

@property (nonatomic, strong) NSMutableArray *goodsData;

@end
